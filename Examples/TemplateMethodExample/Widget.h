//
// Created by Stephen Clyde on 4/12/17.
//

#ifndef TEMPLATEMETHODEXAMPLE_WIDGET_H
#define TEMPLATEMETHODEXAMPLE_WIDGET_H

#include "Component.h"

class Widget : public Component {
private:
    double  m_height  = 0;

public:
    Widget(int id, std::string title, double height) : Component(id, title), m_height(height) {}

    void printDetails(std::ostream& out);

};


#endif //TEMPLATEMETHODEXAMPLE_WIDGET_H
