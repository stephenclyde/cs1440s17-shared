//
// Created by Stephen Clyde on 4/12/17.
//

#ifndef TEMPLATEMETHODEXAMPLE_COMPONENT_H
#define TEMPLATEMETHODEXAMPLE_COMPONENT_H

#include <string>
#include <ostream>

class Component {

private:
    int m_id;
    std::string m_title;

public:
    Component(int id, std::string title) : m_id(id), m_title(title) {}
    int getId() const { return m_id; }
    std::string getTitle() const { return m_title; }

    void print(std::ostream& out);

protected:
    virtual void printHeader(std::ostream& out);
    virtual void printDetails(std::ostream& out);
};


#endif //TEMPLATEMETHODEXAMPLE_COMPONENT_H
