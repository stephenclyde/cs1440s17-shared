//
// Created by Stephen Clyde on 4/12/17.
//

#include "Component.h"

void Component::print(std::ostream& out)
{
    out << "Component" << std::endl;

    printHeader(out);

    out << std::endl;

    printDetails(out);

    out << std::endl << std::endl;
}

void Component::printHeader(std::ostream& out)
{
    out << "id=" << m_id << std::endl;
    out << "title=" << m_title << std::endl;
}

void Component::printDetails(std::ostream& out) { }