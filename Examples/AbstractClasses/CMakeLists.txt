cmake_minimum_required(VERSION 3.6)
project(AbstractClasses)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp PrintableThing.cpp PrintableThing.h Widget.cpp Widget.h Gadget.cpp Gadget.h)
add_executable(AbstractClasses ${SOURCE_FILES})