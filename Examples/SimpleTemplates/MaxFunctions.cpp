//
// Created by Stephen Clyde on 3/20/17.
//


int max(int a, int b)
{
    int result = a;
    if (b>a)
        result = b;
    return result;
}

long max(long a, long b)
{
    long result = a;
    if (b>a)
        result = b;
    return result;
}

float max(float a, float b)
{
    float result = a;
    if (b>a)
        result = b;
    return result;
}

double max(double a, double b)
{
    double result = a;
    if (b>a)
        result = b;
    return result;
}