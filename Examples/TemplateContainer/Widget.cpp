//
// Created by Stephen Clyde on 3/20/17.
//

#include "Widget.h"

Widget::Widget(int id, double value) : m_id(id), m_value(value) {}

void Widget::print(std::ostream &out)
{
    out << "Widget: " << getId()
              << "," << getValue()
              << std::endl;
}